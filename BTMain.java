/**
 * Package backtracking :  
 * Demonstration zum Backtracking-Mechanismus am Beispiel der
 * "Wolf-Ziege-Kohlkopf"-Aufgabenstellung.
 * 
 * fhdwbap/2019
 */
package backtracking;

 

/**
 * BTMain als formale Klasse für die Backtracking-Demo.
 * Wegen des Einsatzes im 1.Semester ist die Implementierung nicht stark 
 * objektorientiert ausgerichtet. Basierend auf einem strukturierten C-Programm 
 * werden hier entsprechend eher strukturähnliche Datentypen eingesetzt.
 * @author fhdwbap
 *
 */
public class BTMain 
{
	public static final int MAXVERTICES = 16; // maximale Anzahl an Knoten 
	
	private static final String PROGTITLE = "Backtracker Demo";
	private static final String VERSION = "0.0.1";
	private static final String COPYRIGHT = "(c) 2019 FHDW Programmer's Group";

	
	/**
	 * @param args Hier nicht genutzt; generell die Kommandozeilenparameter für die main()-Methode
	 */
	public static void main(String[] args) 
	{
		showHeader();
		
		Graph graph = new Graph();
		graph.show(); // Ausgabe des Graphen
		
		if (!graph.isEmpty())
		{
			System.out.println("\n\nBacktracker-Demo wird gestartet.");
			
			// Es wird beim ersten Knoten gestartet...
			backtracker(graph.getV(0),0);
		}
		else
		{
			System.out.println("Der Graph hat leider nichts zu bieten...");
		}
		
		System.out.println("Ende des Programms.");
	}

	/*
	 * Die inhaltliche Hauptmethode: der Backtracking-Algorithmus beginnt bei
	 * dem Knoten v und durchsucht (in rekursiver Form) von dort aus alle
	 * direkten und indirekten Nachfolgeknoten. 
	 * Endet ein Weg, so wird via Backtracking solange wieder ein Schritt zurück
	 * gegangen, bis bei irgendeinem Knoten eine noch nicht besuchte Verzweigung
	 * gefunden wird.
	 * Wird schlussendlich wieder beim Startknoten angekommen keine weitere
	 * Verzweigung gefunden, ist das Durchforsten des Graphen beendet.
	 */
	private static void backtracker(Vertice v, int level)
	{
		// System.out.println("backtracker(" + level + ") gestartet mit " + v );
		
		// Im konkreten Kontext nicht erforderlich, generell aber sinnvolle
		// Vorprüfung
		if (v == null)
		{
			indent(level);
			System.out.println("backtracker(" + level + "): Fehler! Aufruf mit null-Knoten!");
			return;
		}
		
		indent(level);
		System.out.print("backtracker(" + level + "): Untersuche ");
		v.showFlat(); // "Flache" Ausgabe eines Knoten, also ohne dessen Nachfolger
		
		if (v.numberOfNeighbours()==0)
		{
			indent(level);
			System.out.println("backtracker(" + level + "): Keine Nachfolge-Knoten!");
		}
		
		for (Vertice v1 : v.getNeighbour())
		{
			if (v1 != null)
			{
				backtracker(v1,level+1); // rekursiver Aufruf
			}
			 
		}
		
		indent(level);
		System.out.println("backtracker(" + level + "): Ende ");
	}
	
	// --- Interne (private) Hilfsmethoden --- 
	/*
	 * Einrücken um einige Leerzeichen; wird verwendet, um die rekursive
	 * Aufrufhierarchie visuell wiederzugeben.
	 */
	private static void indent(int num)
	{
		for (int i=0; i<num; i++)
		{
			System.out.print("   ");
		}
	}
	
	/*
	 * Anzeige der Programminformationen. Nicht besonders spannend... :)
	 */
	private static void showHeader()
	{
		System.out.println(
				"--------------------------------\n" + 
				PROGTITLE + " " + VERSION + "\n" + 
				COPYRIGHT + "\n" +
				"--------------------------------");
	}
	
} // end class BTMain 

